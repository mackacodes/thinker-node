const express = require('express')
const router = express.Router();
const passport = require('passport')
const jwt = require('jsonwebtoken')
// const config = require('../config/database');
const Project = require('../models/project');
const Task = require('../models/task');
var session = require('express-session')

// Projects list
router.get('/:id', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    var projectId = req.params.id;
    Task.getAllTasks(projectId, (err, tasks) => {
        if (err) {
            res.json({ success: false, message: "Getting tasks failed: "+err })
        } else {
            res.json({ success: true, tasks: tasks })
        }
    })
})

router.post('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let projectId = req.body.projectId
    Project.getProjectById(projectId, (err, result) => {
        console.log(result)
        if (!err) {
            fetchedProject = result
            let newTask = new Task({
                name: req.body.name,
                createdDate: req.body.createdDate,
                status: req.body.status,
                taskDescription: req.body.taskDescription,
                project: fetchedProject,
                createdBy: req.user,
                assignedUser: req.body.assignedUser,
                taskNumber: req.body.taskNumber
            })
            
            Task.addTask(fetchedProject, newTask, (err, task) => {
                console.log('adding task')
                if (err) {
                    res.json({ success: false, message: "Add task failed: "+ err})
                } else {
                    res.json({ success: true, message: "Add task done"})
                }
            })
        }
    })
    
})

router.put('/:id/edit', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    const tobeUpdateTask = {
        projectId: req.params.id,
        taskId: req.body.taskId,
        name: req.body.name,
        taskDescription: req.body.taskDescription,
        status: req.body.status,
        assignedUser: req.body.assignedUser
    }
    Task.editTask(tobeUpdateTask, (err, response) => {
        if (err) {
            res.json({ success: false, message: "Update task failed" })
        } else {
            res.json({ success: true, message: "Update task done" })
        }
    })
})

router.delete('/:id', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let tobeDeletedProject = new Project({
        _id: req.params.id,
    })
    Project.deleteProject(tobeDeletedProject, (err, data) => {
        if (err) {
            res.json({ success: false, message: "Delete project failed" })
        } else {
            res.json({ success: true, message: "Delete project success" })
        }
    })
})

module.exports = router;