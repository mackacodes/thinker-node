const express = require('express')
const router = express.Router();
const passport = require('passport')
const jwt = require('jsonwebtoken')
const config = require('../config/database');
const constants = require('../constants');
const Message = require('../models/message');
const User = require('../models/user');

// Projects list
router.get('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Message.getAllMessages(req.user, (err, messages) => {
        if (err) {
            res.json({ success: false, message: "Getting messages failed" })
        } else {
            res.json({ success: true, messages: messages })
            // res.render('/projects', { success: true, projects: projects })
        }
        
    })
})

router.post('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let newMessage = new Message({
        message: req.body.message,
        emotion: req.body.emotion,
        user: req.user,
    })
    Message.addMessage(req.user, newMessage, res, (err, message) => {
        if (err) {
            res.json({ success: false, message: "Add message failed", error: err })
        } else {
            res.json({ success: true, message: "Add message success", object: message })
        }
    })
})

router.get('/:id', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    var projectId = req.params.id;
    Message.getMessageById(projectId, (err, result) => {
        if (err) {
            res.json( {success: false, message:'No project found with id: '+projectId} )
        } else {
            res.json( {success: true, project: result} )
        }
    })
    
})

module.exports = router;