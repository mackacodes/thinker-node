const express = require('express')
const router = express.Router()
const User = require('../models/user')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const config = require('../config/database')
const constants = require('../constants')
var session = require('express-session')


// User list
router.get('/', (req, res, next) => {
    User.getAllUsers((err, users) => {
        if (!err) {
            res.send(users)
        } else {
            res.json({ success: false, message: "Unable to get user list" })
        }
    })
})

// Register
router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
    })
    User.addUser(newUser, (err, user) => {
        if (err) {
            res.json({ success: false, message: "User registration failed" })
        } else {
            res.json({ success: true, message: "User registration success" })
        }
    })
})

// Authenticate
router.post('/authenticate', (req, res, next) => {
    const username = (req.body.username);
    const password = (req.body.password);

    User.getUserByUsername(username, (err, user) => {
        if (err) console.log('auth user error: ' + err)
        if (!user) {
            return res.json({ success: false, message: "User not found" })
        } 

        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) { console.log('Password match error: ' + err); }
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 6000
                })
                res.json({
                    success: true,
                    token: 'JWT ' + token,
                    message: "",
                    user: {
                        id: user._id,
                        name: user.name,
                        email: user.email
                    }
                })
            } else {
                res.json({ success: false, message: "Wrong password" })
            }
        })
    })
})

// Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    res.json({user: req.user})
})

// GET /logout
router.get('/logout', function(req, res, next) {
    if (req.session) {
      req.session.destroy(function(err) {
        if(err) {
          return next(err);
        } else {
          return res.redirect('/login');
        }
      });
    }
  });

module.exports = router;