import { Injectable } from '@angular/core';
import { Http, Headers, Response, HttpModule} from '@angular/http';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

import 'rxjs/add/operator/map'
import { Observable }   from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Project } from '../models/project.model';


@Injectable()
export class ProjectService {
  // private serviceUrl = 'https://jsonplaceholder.typicode.com/users';
  constructor(
    private http: Http, 
    private authService: AuthService,
    private httpClient: HttpClient
  ) { }

  authToken: any
  user: any

  addProject(project) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.post('projects', project, {headers: headers}).map(res=> res.json())
    return this.http.post('http://localhost:3000/projects', project, {headers: headers}).map(res => res.json())
  }

  // getAllProjects() {
  //   // this.loadToken()
  //   let headers = new Headers();
  //   headers.append('Authorization',this.authService.authToken)
  //   headers.append('Content-Type','application/json')
  //   // return this.http.get('projects', {headers: headers}).map(res => res.json())
  //   return this.http.get('http://localhost:3000/projects', {headers: headers}).map(res => res.json())
  // }

  getAllProjects(): Observable<Project[]> {
    this.authService.loadToken()
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': this.authService.authToken
      })
    };
    // return this.httpClient.get<Project[]>('projects', httpOptions)
    return this.httpClient.get<Project[]>('http://localhost:3000/projects', httpOptions)
  }

  getProject(project_id) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.get('projects/'+project_id, {headers: headers}).map(res => res.json())
    return this.http.get('http://localhost:3000/projects/'+project_id, {headers: headers}).map(res => res.json())
  }

  editProject(project) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.put('projects/'+project._id+'/edit', project,{headers: headers}).map(res => res.json())
    return this.http.put('http://localhost:3000/projects/'+project._id+'/edit', project,{headers: headers}).map(res => res.json())
  }

  updateProjectSprint(project) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.put('projects/'+project._id+'/updatesprint', project, {headers: headers}).map(res => res.json())
    return this.http.put('http://localhost:3000/projects/'+project._id+'/updatesprint', project, {headers: headers}).map(res => res.json())
  }

  shareProject(data) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.put('projects/'+data.projectId+'/share', data, {headers: headers}).map(res => res.json())
    return this.http.put('http://localhost:3000/projects/'+data.projectId+'/share', data, {headers: headers}).map(res => res.json())
  }

  deleteProject(project) {
    this.authService.loadToken()
    // console.log('proj service payl: 'project.id)
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.delete('projects/'+project.id, {headers: headers}).map(res => res.json())
    return this.http.delete('http://localhost:3000/projects/'+project.id, {headers: headers}).map(res => res.json())
  }
  // getUser(): Observable<Project[]> {
  //   return this.http.get<Project[]>(this.serviceUrl);
  // }
}
