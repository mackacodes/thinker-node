import { Injectable } from '@angular/core';
import { Http, Headers, Response, HttpModule} from '@angular/http';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

import 'rxjs/add/operator/map'
import { Observable }   from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Project } from '../models/project.model';

@Injectable()
export class TaskService {

  constructor(
    private http: Http, 
    private authService: AuthService,
    private httpClient: HttpClient
  ) { }

  authToken: any
  user: any

  addTask(task) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.post('tasks', task, {headers: headers}).map(res => res.json())
    return this.http.post('http://localhost:3000/tasks', task, {headers: headers}).map(res => res.json())
  }

  getAllTasks(projectId) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.get('tasks/'+projectId, {headers: headers}).map(res => res.json())
    return this.http.get('http://localhost:3000/tasks/'+projectId, {headers: headers}).map(res => res.json())
  }

  updateTask(task) {
    this.authService.loadToken()
    let headers = new Headers();
    headers.append('Authorization',this.authService.authToken)
    headers.append('Content-Type','application/json')
    // return this.http.put('tasks/'+task.projectId+'/edit', task, {headers: headers}).map(res => res.json())
    return this.http.put('http://localhost:3000/tasks/'+task.projectId+'/edit', task, {headers: headers}).map(res => res.json())
  }
}
