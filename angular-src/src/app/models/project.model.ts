export interface Project {
    _id: String,
    name: String;
    createdDate: String;
    users: Object;
    isActive: Boolean;
    projDescription: String;
    createdByName: String,
    projectCode: String
}

export class Project {
    constructor() {

    }
}