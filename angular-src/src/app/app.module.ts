import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes, Route } from '@angular/router';
import {  } from '@angular/animations';

// Material
import { MaterialModule } from './material.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { FooterViewComponent } from './components/footer-view/footer-view.component';

import { AuthGuard } from './guards/auth.guard';
import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service';
import { ProjectService } from './services/project.service';
import { TaskService } from './services/task.service';
import { SimpleDialogComponent } from './components/simple-dialog/simple-dialog.component';

const appRoutes: Routes = [
  {path:'', component:HomeComponent},
  {path:'register', component:RegisterComponent},
  {path:'login', component:LoginComponent},
  // {path:'projects/add', component:SimpleDialogComponent},
  // {path:'projects/:id/edit', component:SimpleDialogComponent, canActivate:[AuthGuard]},
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    SimpleDialogComponent,
    FooterViewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
    ValidateService,
    AuthService,
    ProjectService,
    AuthGuard,
    HttpClientModule,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
