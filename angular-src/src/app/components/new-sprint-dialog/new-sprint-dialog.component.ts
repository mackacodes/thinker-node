import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { } from '../../material.module';
import { ValidateService } from '../../services/validate.service';
import { MatDatepickerModule, MatDatepickerToggle, MatDatepickerInput } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { ValidatorFn, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-new-sprint-dialog',
  templateUrl: './new-sprint-dialog.component.html',
  styleUrls: ['./new-sprint-dialog.component.css']
})
export class NewSprintDialogComponent {

  sprintName: String
  sprintStartingDate: Date
  sprintEndDate: Date

  minimumDate = new Date()

  constructor(public dialogRef: MatDialogRef<NewSprintDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private validateService: ValidateService) {}

  done(): void {
    this.data = {
      sprintName: this.sprintName,
      sprintStartingDate: this.sprintStartingDate,
      sprintEndDate: this.sprintEndDate
    }
    if (this.validateService.validateCreateSprint(this.data)) {
      this.dialogRef.close(this.data);
    }
  }

  setStartTime(date) {
    this.sprintStartingDate = date
  }

  setEndTime(date) {
    this.sprintEndDate = date
  }

}
