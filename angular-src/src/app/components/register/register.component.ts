import { Component, OnInit } from '@angular/core';
import { PassThrough } from 'stream';
import { MaterialModule } from '../../material.module';

import { ValidateService } from '../../services/validate.service'
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

// import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: string
  email: string
  displayName: string
  password: string

  constructor(private vaidateService: ValidateService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    console.log("It's new year season. Happy new year!")
  }

  onRegisterSubmit() {
    const user =  {
      name: this.name,
      email: this.email,
      password: this.password
    }
    
    // required fields
    if (!this.vaidateService.validateRegisterDetails(user)) {
      window.alert('Please fill in all fields')
      return false
    }

    if (!this.vaidateService.validateEmail(user.email)) {
      window.alert('Please enter a valid email address.')
      return false
    }
    
    // register user
    this.authService.registerUser(user).subscribe(data =>  {
      if (data.success) {
        window.alert('Register success')
        this.router.navigate(['/login'])
      } else {
        window.alert('Register failed')
      }
    })
  }
}
