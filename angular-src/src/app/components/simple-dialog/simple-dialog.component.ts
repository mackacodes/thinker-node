import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { } from '../../material.module';
import { ValidateService } from '../../services/validate.service';


@Component({
  selector: 'app-simple-dialog',
  templateUrl: './simple-dialog.component.html',
  styleUrls: ['./simple-dialog.component.css']
})
export class SimpleDialogComponent {

  name: String
  projectCode: String
  projectDesciption: String
  projectId: any
  isUpdating = false
  isSharingView = false
  
  allUsers: [any]
  assignedUser: any

  constructor(public dialogRef: MatDialogRef<SimpleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validateService: ValidateService
  ) {
    if (data.isUpdating) {
      this.name = data.name
      this.projectCode = data.projectCode
      this.projectDesciption = data.projectDesciption
      this.isUpdating = data.isUpdating
      this.projectId = data.projectId
      console.log('id: ' + data.projectId)

    } else if (data.isSharingView) {
      this.isSharingView = data.isSharingView
      this.allUsers = data.allUsers
    }
  }

  done(): void {
    const newProjData = {
      name: this.name,
      projectDescription: this.projectDesciption,
      projectCode: this.projectCode
    }
    // console.log(newProjData)
    if (this.validateService.validateAddProjectDetails(newProjData)) {
      this.dialogRef.close(newProjData);
    }
  }

  update(): void {
    const updateData = {
      _id: this.projectId,
      name: this.name,
      projectDescription: this.projectDesciption,
      projectCode: this.projectCode
    }
    if (this.validateService.validateAddProjectDetails(updateData)) {
      // console.log(updateData)
      this.dialogRef.close(updateData);
    }
  }

  addUser(): void {
    console.log(this.assignedUser)
    const updateData = {
      userId: this.assignedUser._id,
      userName: this.assignedUser.name
    }
    if (this.assignedUser != undefined) {
      this.dialogRef.close(updateData);
    }
  }
}
