import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap} from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ProjectService } from '../../services/project.service';
import { TaskService } from '../../services/task.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NewSprintDialogComponent } from '../new-sprint-dialog/new-sprint-dialog.component';
import { NewTaskComponent } from '../new-task/new-task.component';


@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {

  constructor(
    public route:ActivatedRoute, 
    public projectService: ProjectService,
    private router: Router,
    public dialog: MatDialog,
    public taskService: TaskService
  ) { }

  projectId: any
  selectedProject: any
  hasSprint: boolean
  currentSprintName: string
  taskCount = 0


  projectUsers: [{userId: String, userName: String}]
  todoTasks = []
  inProgressTasks = []
  toReviewTasks = []
  doneTasks = []

  sprintStartingDate: Date
  sprintEndDate: Date

  ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get('id');
    this.getSelectedProject()
    this.getAllTasks()
  }

  getSelectedProject() {
    this.projectService.getProject(this.projectId).subscribe(data => {
      if (data.success) {
        this.selectedProject = data.project
        if (this.selectedProject.sprintName != undefined) {
          this.hasSprint = true
          this.currentSprintName = this.selectedProject.sprintName
          this.taskCount = this.selectedProject.tasks.length
          this.projectUsers = this.selectedProject.users
        } else {
          this.hasSprint = false
        }
      } else {
        this.router.navigate(['/projects'])
      }
    })
  }

  getAllTasks() {
    this.todoTasks = []
    this.inProgressTasks = []
    this.toReviewTasks = []
    this.doneTasks = []

    this.taskService.getAllTasks(this.projectId).subscribe(data => {
      if (data.success) {
        this.taskCount = data.tasks.length
        for (let element of data.tasks) {
          if (element.status == "To Do") {
            this.todoTasks.push(element)
          } else if (element.status == "In Progress") {
            this.inProgressTasks.push(element)
          } else if (element.status == "To Review") {
            this.toReviewTasks.push(element)
          } else if (element.status == "Done") {
            this.doneTasks.push(element)
          }
        }
      }
    })
  }

  openCreateSprintDialog(): void {
    let dialogRef = this.dialog.open(NewSprintDialogComponent, {
      width: '600px',
      minWidth: '300px',
      height: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        // console.log('Dialog result: '+result.name);
        // console.log('Dialog result: '+result.projectDesciption);
        // this.currentSprintName = result.sprintName
        this.sprintStartingDate = result.sprintStartingDate
        this.sprintEndDate = result.sprintEndDate

        this.selectedProject.sprintName = result.sprintName
        this.selectedProject.sprintStartDate = result.sprintStartingDate.toISOString()
        this.selectedProject.sprintEndDate = result.sprintEndDate.toISOString()

        // console.log(result.sprintStartingDate.toISOString())
        // console.log(result.sprintEndDate.toISOString())

        this.updateProject()
      }
    });
  }

  openCreateTaskDialog(): void {
    let dialogRef = this.dialog.open(NewTaskComponent, {
      width: '600px',
      minWidth: '300px',
      height: '450px',
      data: { 
        newTaskNumber: this.selectedProject.projectCode +'-'+ (this.taskCount+1), 
        projectUsers: this.projectUsers 
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.createTask(result)
      }
    });
  }

  openUpdateTaskDialog(task): void {
    let dialogRef = this.dialog.open(NewTaskComponent, {
      width: '600px',
      minWidth: '300px',
      height: '500px',
      data: { 
        isUpdating: true,
        name: task.name,
        taskDescription: task.taskDescription,
        newTaskNumber: task.taskNumber,
        projectUsers: this.projectUsers,
        selectedStatus: task.status,
        assignedUser: task.assignedUser
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.updateTask(task._id, result)
      }
    });
  }

  updateProject() {
    // console.log(this.selectedProject.sprintStartingDate)
    const updateData = {
      _id: this.selectedProject._id,
      sprintName: this.selectedProject.sprintName,
      sprintStartDate: this.selectedProject.sprintStartDate,
      sprintEndDate: this.selectedProject.sprintEndDate
    }
    this.projectService.updateProjectSprint(updateData).subscribe(data => {
      if (data != undefined) {
        this.getSelectedProject()
      }
    })
  }

  createTask(result) {
    const newTask = {
      name: result.name,
      taskDescription: result.taskDescription,
      assignedUser: result.assignedUser,
      taskNumber: result.taskNumber,
      projectId: this.projectId,
      status: 'To Do',
      createdDate: new Date(Date.now()).toISOString(),
    }
    this.taskService.addTask(newTask).subscribe(data => {
      if (data.success) {
        this.getAllTasks()
      }
    })
  }

  updateTask(taskId, result) {
    const newTask = {
      taskId: taskId,
      name: result.name,
      taskDescription: result.taskDescription,
      assignedUser: result.assignedUser,
      taskNumber: result.taskNumber,
      projectId: this.projectId,
      status: result.status,
    }
    this.taskService.updateTask(newTask).subscribe(data => {
      if (data.success) {
        this.getAllTasks()
      }
    })
  }
  
}
