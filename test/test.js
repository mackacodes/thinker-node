const assert = require('chai').assert;
const config = require('../config/database');

const Project = require('../models/project');
const Projects = require('../routes/projects');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

let should = chai.should();
let expect = chai.expect;

const token = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9qZWN0cyI6WyI1YjI2M2JlZjU3MzY2Yzk1YjNjNjJlOTAiLCI1YjI3NGUwOWE4YTU1ZmQ5NjliYmEwMzMiXSwiX2lkIjoiNWIyNjNiZGM1NzM2NmM5NWIzYzYyZThmIiwibmFtZSI6IkhpbWFsIE1hZGh1c2hhbiIsImVtYWlsIjoicXdlQGdtYWlsLmNvbSIsInVzZXJuYW1lIjoicXdlIiwicGFzc3dvcmQiOiIkMmEkMTAkQTNUMVdVcDRRZnpLWHp6WkV4QU43T1NUZjhCMmI4Z3lUNC8zS21rc05oMWNWay82bmtSTnUiLCJfX3YiOjIsImlhdCI6MTUyOTMwMzc2NywiZXhwIjoxNTI5MzYzNzY3fQ.0l9R_DmwO4yU5ZOpuLGH4X1Ha628MJSv0YS0ignSsTk'

it('post - create project', function (done) {
    chai.request('http://localhost:3000/projects')
        .post('/')
        .send({
            name: "test proj",
            createdDate: "2018-06-12",
            createdBy: "5b22317d932c193ec6f4c683",
            createdByName:"Himal Madhushan",
            projDescription: "sample description",
            projectCode:"SMPL",
            isActive:true,
            sprintName:"Sprint 01",
            sprintStartDate:"2018-06-17",
            sprintEndDate:"2018-06-19"

        })
        .end(function (err, res) {
            // console.log(err)
            expect(res.status).to.equal(200);
            // expect(res.body.success).to.equal(true);

            done();
        });
});

//update project
it('put - edit project', function (done) {
    chai.request('http://localhost:3000/projects')
        .put('/5b223f50bebbef42a1f7f63b/edit')
        .send({
            "_id": "5b223f50bebbef42a1f7f63b",
            "name": "updated",
            "projDescription": "puh descrip"

        })
        .end(function (err, res) {

            expect(res.statusCode).to.equal(200);
            //expect(res.body).to.equal(true);

            done();
        });
});

//get projects
it('get - all projects', (done) => {
    chai.request('http://localhost:3000/projects')
        .get('/')
        .set('Authorization', token)
        .end((err, res) => {
            // console.log(res)
            expect(res).to.have(200);
            //  expect(res.body.success).to.equal(true);

            done();
        });
});