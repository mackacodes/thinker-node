const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const TaskSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        required: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    project: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Project'
    },
    assignedUser: {
        userId: String, 
        userName: String
    },
    status: {
        type: String,
        required: true,
        enum: ["To Do", "In Progress", "To Review", "Done"]
    },
    taskDescription: {
        type: String,
    },
    taskNumber: {
        type: String,
        required: true
    }
})

const Task = module.exports = mongoose.model('Task', TaskSchema)

module.exports.getTaskById = function (id, callback) {
    Task.findById(id, callback)
}

module.exports.getAllTasks = function (projectId, callback) {
    // Project.findOne(callback)
    const condition = {
        project: projectId.toString() 
    }
    Task.find(condition, callback)
}

// module.exports.getUserByEmail = function (email, callback) {
//     const query = { email: email };
//     User.findOne(query, callback)
// }

module.exports.addTask = function (fetchedProject ,newTask, callback) {
    fetchedProject.tasks.push(newTask)
    fetchedProject.save()
    // newTask.project.tasks.push(newTask)
    newTask.save(callback)
}

module.exports.deleteProject = function (tobeDeletedTask, callback) {
    Task.find({ _id:tobeDeletedTask.id }, callback).remove().exec()
}

module.exports.editTask = function (task, callback) {
    Task.findByIdAndUpdate(task.taskId, { 
        $set: { 
            name: task.name, 
            taskDescription: task.taskDescription, 
            status: task.status,
            assignedUser: task.assignedUser
        }
    }, callback);
}