const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const MessageSchema = mongoose.Schema({
    message: {
        type: String,
        required: true
    },
    emotion: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        default: new Date(Date.now()).toISOString(),
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
})

const Message = module.exports = mongoose.model('Message', MessageSchema)

module.exports.getMessageById = function (id, callback) {
    Message.findById(id, callback)
}

module.exports.getAllMessages = function (user, callback) {
    const condition = {
        user: user._id.toString()
    }
    Message.find(condition, callback)
}

module.exports.addMessage = function (user, newMessage, res, callback) {
    user.messages.push(newMessage)
    user.save()
    newMessage.user = user
    newMessage.save()
    res.json({ success: true, message: "Add message success" })
}